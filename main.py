from tkinter import *
from tkinter import messagebox

import serial.tools.list_ports
import functools
from csv import reader, writer

ports = serial.tools.list_ports.comports()
serialObject = serial.Serial()

window = Tk()
window.config(bg='grey')
window.title('Rs232 receiver')
window.attributes('-topmost',True)


def appendToFile(fileName, recentPacketString):
    with open(fileName, 'a', encoding='utf8', newline='') as output_file:
        output_file.write(f'{recentPacketString}\n')

def serialStart(comPortVar):
    serialObject.port = comPortVar
    serialObject.baudrate = 115200
    try:
        serialObject.open()
    except ValueError:
        messagebox.showinfo('Can\'t open COM port')

def initComPort(index):
    courrentPort =str(ports[index])
    # print(courrentPort)
    comPortVar = str(courrentPort.split(' ')[0])
    print(comPortVar)
    if not serialObject.isOpen():
        serialStart(comPortVar)
    else:
        serialObject.close()
        serialStart(comPortVar)


for onePort in ports:
    comButton = Button(window, text=onePort, font=('Calibri', '13'), height=1, width=45, command=functools.partial(initComPort, index=ports.index(onePort)))
    comButton.grid(row=ports.index(onePort), column=0)

dataCanvas = Canvas(window, width=600, height=400, bg='white')
dataCanvas.grid(row=0, column=1, rowspan=100)

vsb = Scrollbar(window, orient='vertical', command=dataCanvas.yview())
vsb.grid(row=0, column=2, rowspan=100, sticky='ns')

dataCanvas.config(yscrollcommand=vsb.set)
dataFrame=Frame(dataCanvas, bg='white')
dataCanvas.create_window((10, 0), window=dataFrame, anchor='nw')

def checkSerialPort():
    if serialObject.isOpen() and serialObject.in_waiting:
        recentPacket=serialObject.readline()
        recentPacketString=recentPacket.decode('utf8').rstrip('\n')
        appendToFile('Measurements.csv', recentPacketString)
        Label(dataFrame, text=recentPacketString, font=('Calibri', 13), bg='white').pack()

while True:
    window.update()
    checkSerialPort()
    dataCanvas.config(scrollregion=dataCanvas.bbox("all"))